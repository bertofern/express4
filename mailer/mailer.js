require('dotenv').config();
const nodemailer = require('nodemailer')
// const sgTransport = require('nodemailer-sendgrid-transport')
const nodemailerSendgrid = require('nodemailer-sendgrid');

let mailConfig

if (process.env.NODE_ENV === "production") {
    // const options = {
    //     auth: {
    //         api_key: process.env.SENDGRID_API_SECRET,
    //     },
    // }
    // mailConfig = sgTransport(options)
    const transport = nodemailer.createTransport(
        nodemailerSendgrid({
            apiKey: process.env.SENDGRID_API_SECRET
        })
    );
    module.exports = transport;
} else {
    if (process.env.NODE_ENV === "staging") {
        // const options = {
        //     auth: {
        //         api_key: process.env.SENDGRID_API_SECRET,
        //     },
        // };
        // mailConfig = sgTransport(options)
        const transport = nodemailer.createTransport(
            nodemailerSendgrid({
                apiKey: process.env.SENDGRID_API_SECRET
            })
        );
        module.exports = transport;
    } else {
        mailConfig = {
            host: "smtp.ethereal.email",
            port: 587,
            auth: {
                user: process.env.ETHEREAL_USER_DEV,
                pass: process.env.ETHEREAL_PASS_DEV
            },
        }
        module.exports = nodemailer.createTransport(mailConfig)

        // mailConfig = {
        //     host: process.env.MAIL_HOST,
        //     port: process.env.MAIL_PORT,
        //     auth: {
        //         user: process.env.MAIL_USERNAME,
        //         pass: process.env.MAIL_PASSWORD
        //     },
        // }
    }
}

// module.exports = nodemailer.createTransport(mailConfig)