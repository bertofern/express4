[![NodeJs Logo](public/images/nodejs.png)](https://nodejs.org/)
[![Express Logo](public/images/express.png)](http://expressjs.com/)
[![Mongoose Logo](public/images/mongoose.png)](https://mongoosejs.com/)
[![Jasmine Logo](public/images/jasmine.png)](https://jasmine.github.io/)
[![Heroku Logo](public/images/heroku.png)](https://heroku.com/)
[![Mongodb Logo](public/images/mongodb.png)](https://www.mongodb.com/cloud/atlas)
[![Sendgrid Logo](public/images/sendgrid.png)](https://nodemailer.com/)
[![Sendgrid Logo](public/images/nodemailer.png)](https://sendgrid.com/)
[![Leafletjs Logo](public/images/leafletjs.png)](https://leafletjs.com/)
[![PassportJs Logo](public/images/passport.png)](http://www.passportjs.org/)
[![NewRelic Logo](public/images/newrelic.png)](https://newrelic.com/)


About me: [bertofern](https://bertofern.wordpress.com/)

### Desarrollo del lado servidor NodeJS, Express y MongoDB para el Curso "Full Stack Web Development" - Universidad Austral - coursera.org 

## Instación y Uso

**Configurar variables de Entorno con el paquete 'dotenv'**

*Copiar el archivo .envExample a .env y añadir las variables a este archivo.*

*Y aseguarse de que .gitignore ignora .env .*

*mailer/mailer está configurado con el servicio de pruebas: https://ethereal.email/ .*

*ETHEREAL USER DEV y ETHEREAL PASS DEV son las variables para este servicio de testeo de mail.*


**Para arrancar el servidor de la API con Express:**

```
$ npm install

$ npm start
```
*Se interactuará con la API en: http://localhost:3000/*


**Para arrancar el servidor de la API con Express y Nodemon:**

```
$ npm run devstart
```


**Para arrancar el servidor de la API con Express, Nodemon y Debug:**

```
$ DEBUG=* npm run devstart 
```


---


## Pruebas Unitarias con Jasmine

**Para correr todos los test**
```
$ npm test
```


**Para correr todos los test uno por uno**
```
$ jasmine spec/models/bicicleta_test.spec.js
$ jasmine spec/models/usuario_test.spec.js
$ jasmine spec/api/bicicleta_api_test.spec.js
$ jasmine spec/api/usuario_api_test.spec.js
```


---


## Authenticate API

**Crear una Token para utilizarlo en las partes privadas de la API - Method: POST**

*URL: http://localhost:3000/api/auth/authenticate*

*URL: https://bicicletas-api-express4.herokuapp.com/api/auth/authenticate*

*Headers: Content-Type => application/json*

```
{
    "email": "example@gmail.com",
    "password":"Ex4mple-PA33word"
}
```

*Respuesta:*

```
{
    "message": "usuario encontrado!",
    "data": {
        "usuario": {
            "verificado": true,
            "_id": "5f8f4f448e4caf249067f293",
            "nombre": "example",
            "email": "example@gmail.com",
            "password": "$2b$10$rbXCqpttJDg6or/l3pkioe4SFyJ/2le9pMKX8naE2WEwt9ccJZdei",
            "__v": 0
        },
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmOGY0ZjQ0OGU0Y2FmMjQ5MDY3ZjI5MyIsImlhdCI6MTYwMzMwNTE5OSwiZXhwIjoxNjAzOTA5OTk5fQ.XU1x-VEN9u5Ndvl74sAKdMV6mawquLE2gJIq1q4Lbl8"
    }
}
```

---

## Authenticate API con Facebook

**Crear una Token de una cuenta Facebook para utilizarlo en las partes privadas de la API - Method: POST**

*URL: http://localhost:3000/api/auth/facebook_token*

*URL: https://bicicletas-api-express4.herokuapp.com/api/auth/facebook_token*

*Headers: Content-Type => application/json*

*Primero es necesario optener el "access token" de tu cuenta o de una cuenta de prueba en https://developers.facebook.com/*

```
{
    "access_token":"EAAFEAVU6mCEBAODbm1ttZBziiZBPY8ffh5YSwkdM2i5DTa9fyOQcykgjF1p1V16fKfxqYVSjCCZCoBsEYvuxkr0ZBDEYxl9IfRfeNO5qmfl3zjZCTfcIcqC8GwxH6vhr2fGdZAH8RUv1wwhNFoOyZCRZBZC2HjPLfzucrPu0tRk62L81fzRIcvgM5pHX6GwYlciRXIgfjZCAtnE37AH4IMQpiW"
}
```
Respuesta:
```
{
    "message": "usuario encontrado!",
    "data": {
        "usuario": {
            "verificado": true,
            "_id": "5f8f4f448e4caf249067f293",
            "nombre": "example",
            "email": "example@gmail.com",
            "password": "$2b$10$rbXCqpttJDg6or/l3pkioe4SFyJ/2le9pMKX8naE2WEwt9ccJZdei",
            "__v": 0
        },
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmOGY0ZjQ0OGU0Y2FmMjQ5MDY3ZjI5MyIsImlhdCI6MTYwMzMwNTE5OSwiZXhwIjoxNjAzOTA5OTk5fQ.XU1x-VEN9u5Ndvl74sAKdMV6mawquLE2gJIq1q4Lbl8"
    }
}
```

---

## Ejemplos API con MongoDB

**Ver bicicletas - Method: GET**

*Header: x-access-token => EJEMPLO_TOKEN_eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmOGY0ZjQ0OGU0Y2FmMjQ5MDY3ZjI5MyIsImlhdCI6MTYwMzMwNTE5OSwiZXhwIjoxNjAzOTA5OTk5fQ.XU1x-VEN9u5Ndvl74sAKdMV6mawquLE2gJIq1q4Lbl8*

*URL: http://localhost:3000/api/bicicletas*

*URL: https://bicicletas-api-express4.herokuapp.com/api/bicicletas*


**Crear una nueva bicicleta - Method: POST**

*Header: Content-Type => application/json*

*Header: x-access-token => EJEMPLO_TOKEN_eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmOGY0ZjQ0OGU0Y2FmMjQ5MDY3ZjI5MyIsImlhdCI6MTYwMzMwNTE5OSwiZXhwIjoxNjAzOTA5OTk5fQ.XU1x-VEN9u5Ndvl74sAKdMV6mawquLE2gJIq1q4Lbl8*

*URL: http://localhost:3000/api/bicicletas/create*

*URL: https://bicicletas-api-express4.herokuapp.com/api/bicicletas/create*
```
{
    "code":4,
    "color":"Black",
    "modelo":"Trek",
    "lat": 42.268114,
    "lng": 2.969612
}
```

**Modificar una bicicleta - Method: POST**

*Header: Content-Type => application/json*

*Header: x-access-token => EJEMPLO_TOKEN_eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmOGY0ZjQ0OGU0Y2FmMjQ5MDY3ZjI5MyIsImlhdCI6MTYwMzMwNTE5OSwiZXhwIjoxNjAzOTA5OTk5fQ.XU1x-VEN9u5Ndvl74sAKdMV6mawquLE2gJIq1q4Lbl8*

*URL: http://localhost:3000/api/bicicletas/4/update*

*URL: https://bicicletas-api-express4.herokuapp.com/api/bicicletas/4/update*

```
{
    "_id": "5f8cc76eefaaa331286131c8",
    "code":4,
    "color":"Pink",
    "modelo":"Cannon",
    "lat": 42.268114,
    "lng": 2.969612
}
```


**Eliminar una nueva bicicleta - Method: DELETE**

*Header: Content-Type => application/json*

*Header: x-access-token => EJEMPLO_TOKEN_eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmOGY0ZjQ0OGU0Y2FmMjQ5MDY3ZjI5MyIsImlhdCI6MTYwMzMwNTE5OSwiZXhwIjoxNjAzOTA5OTk5fQ.XU1x-VEN9u5Ndvl74sAKdMV6mawquLE2gJIq1q4Lbl8*

*URL: http://localhost:3000/api/bicicletas/delete*

*URL: https://bicicletas-api-express4.herokuapp.co/api/bicicletas/delete*
```
{
    "code": 4
}
```


---


## Acerca del proyecto

*Prácticas del Módulo 1:*

* Crear un proyecto de Node JS.
* Resguardar tu proyecto con GIT sobre Bitbucket.
* Configurar librerías con NPM.
* Aplicar los conceptos básicos de la programación web y Express.

*Puntos:*

1. Un archivo README (sólo texto) en el repositorio de Bitbucket.
2. El mensaje de bienvenida a Express.
3. El proyecto vinculado con el repositorio de Bitbucket creado previamente.
4. El servidor que se ve correctamente, comparándolo con la versión original.
5. Un mapa centrado en una ciudad.
6. Marcadores indicando una  ubicación.
7. Un script npm en el archivo package.json, debajo del “start”, que se llama “devstart” y que levanta el servidor utilizando npdemon.
8. Un par de bicicletas en la colección que conoce la bicicleta en memoria, con las ubicaciones cercanas al centro del mapa agregado.
9. Una colección del modelo de bicicleta.
10. Los endpoints de la API funcionando con Postman.

---

*Prácticas del Módulo 2:*

* Configurar una base MongoDB.
* Realizar las típicas tareas CRUD sobre MongoDB.
* Conectar tu modelo con MongoDB utilizando Mongoose.

*Puntos:*

1. Una carpeta con nombre “models” dentro de la carpeta spec.
2. Los tests aprobados
3. Tests de cada operación de la API de bicicleta.
4. Un archivo bicicleta_api_test.spec.js
5. Todos los tests aprobados al ejecutar el comando npm test.
6. Las colecciones actuales Desde Mongo Compass, conectado a tu base local de Mongo.
7. Una base local mongo llamada “red_bicicletas” conectada usando mongoose.
8. El modelo funcional utilizando Postman.
9. Documentos generados en la base local con Mongo Compass.
10. Tests del modelo Bicicleta que usan persistencia.

---

*Prácticas del Módulo 3:*

* Registrar usuarios y autorizarlos.
* Aplicar diferentes formas de autorización utilizando Passport.
* Segurizar una API Rest utilizando JWT.

*Puntos:*

1. Los atributos, con las restricciones correspondientes: email, password, passwordResetToken, passwordResetTokenExpires, verificado.
2. El modelo de Token que debe tener una referencia al usuario asignado, el token propiamente dicho y la fecha de creación.
3. Un email ejecutado en algún paso del proyecto.
4. El email de bienvenida con el link de verificación de cuenta.
5. Vistas de login, recupero de password y demás.
6. Las funciones de serializeUser y deserializeUser en el archivo “passport.js”.
7. Credenciales verdaderas e incorrectas.
8. Que se te niega el acceso al escribir directamente las url en el navegador; y si no estás logueado en el sistema, eres redirigido al login.
9. Utilizando Postman, te autenticas enviando credenciales correctas y obteniendo un token como respuesta.
10. El token válido después de autenticarte y acceder a los recursos de bicicletas utilizando Postman. Y un mensaje de error sin haberte autenticado.

---

*Prácticas del Módulo 4:*

* Integrar OAuth en tu sitio para permitir el login con Google y otros proveedores.
* Publicar tu proyecto en Heroku.
* Monitorear el estado de tus servidores.

*Puntos:*

1. La app publicada a través del servicio de heroku.
2. El usuario creado en la base de mongo, utilizando el visor web o mongo compa
3. Las variables NODE_ENV=”production” y MONGO_URI=”[dirección de conexión con mongo atlas]”
4. El mongo local utilizado correctamente.
5. Que haciendo un deploy a Heroku, la aplicación productiva utiliza mongo atlas.
6. Que el ambiente local sigue enviando los mails por ethereal.
7. Que los emails se envían por Sendgrid (crear un usuario con un email tuyo enviará un email de verificación).
8. El método “findOneOrCreateByGoogle” en el modelo de usuario.
9. La validación del token de Facebook.
10. La librería “newrelic” en app.js el require(‘newrelic’).