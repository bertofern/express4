var map = L.map('mapid', {
    center: [42.2667, 2.9667],
    zoom: 13
});

map.touchZoom.disable();
map.doubleClickZoom.disable();
map.scrollWheelZoom.disable();
map.boxZoom.disable();
map.keyboard.disable();

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
}).addTo(map)

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            var marcador = L.marker(bici.ubicacion, {title: bici.code}).addTo(map);
            marcador.bindPopup(`<b>Bicicleta Code: ${bici.code}</b><br>Modelo: ${bici.modelo}<br>Color: ${bici.color}`).openPopup();
        })
    }
})


// $.ajax({
//     method: 'POST',
//     dataType: 'json',
//     url: 'api/auth/authenticate',
//     data: { email: 'example@gmail.com', password: 'example' },
// }).done(function( data ) {
//     console.log(data);
//     $.ajax({
//         dataType: 'json',
//         url: 'api/bicicletas',
//         beforeSend: function (xhr) {
//             xhr.setRequestHeader("x-access-token", data.data.token);
//         }
//     }).done(function (result) {
//         console.log(result);
//         result.bicicletas.forEach(bici => {
//             L.marker(bici.ubicacion, { title: bici.code }).addTo(mymap);
//             marcador.bindPopup(`<b>Bicicleta Code: ${bici.code}</b><br>Modelo: ${bici.modelo}<br>Color: ${bici.color}`).openPopup();
//         });
//     });
// });