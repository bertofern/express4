var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');

describe('Testing Reserva', () => {

    beforeEach(function(done) {
        mongoose.connect('mongodb://localhost/test', { 
            useUnifiedTopology: true, 
            useFindAndModify: false,
            useCreateIndex: true,
            useNewUrlParser: true
        });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Mongoose connection error:'));
        db.once('open', function() {
          console.log('We are connected to test database MongoDB');
          done();
        });
    });

    afterEach(function(done) {
        Reserva.deleteMany({}, function(err, success){
            if (err) console.log(err);
            Usuario.deleteMany({}, function(err, success){
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success){
                    if (err) console.log(err);
                    done();
                    mongoose.disconnect();
                });
            });
        });
    });


    describe('Cuando el Usuario Reserva una Bicicleta', () => {
        it('Usuario hace la Reserva', (done) => {
            var usuario = new Usuario({nombre: 'Albert Fernánde Martínez'});
            usuario.save();
            var bicicleta = Bicicleta({code: 1, color: 'Black', modelo: 'Orbea', ubicacion: [42.266950, 2.956106]});
            bicicleta.save();

            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate()+1);

            usuario.reservar(bicicleta.id, hoy, mañana, function(err, reserva) {
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas) {
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                    console.log('OK! Usuario Reserva una Bicicleta correctamente');
                });
            });
        });
    });
    

});
