var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', () => {

    beforeEach(function(done) {
        mongoose.connect('mongodb://localhost/test', { 
            useUnifiedTopology: true, 
            useFindAndModify: false,
            useCreateIndex: true,
            useNewUrlParser: true
        });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Mongoose connection error:'));
        db.once('open', function() {
          console.log('We are connected to test database MongoDB');
          done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
            mongoose.disconnect();
        });
    });


    describe('Bicicleta.createInstance', () => {
        it('Crea una Instancia de Bicicleta', () => {
            var a1 = Bicicleta.createInstance(1, 'Black', 'Orbea', [42.266950, 2.956106]);
    
            expect(a1.code).toEqual(1);
            expect(a1.color).toBe('Black');
            expect(a1.modelo).toBe('Orbea');
            expect(a1.ubicacion[0]).toEqual(42.266950);
            expect(a1.ubicacion[1]).toEqual(2.956106);
            console.log('OK! Modelo/Bicicleta.createInstance: Funciona correctamente');
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('Bicicletas empieza vacio', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
                console.log('OK! Modelo/Bicicleta.allBicis: Funciona correctamente');
            });
        });
    });

    // describe('Bicicleta.add', () => {
    //     it('Agrega una Bicicleta', (done) => {
    //         var a1 = Bicicleta.createInstance(1, 'Black', 'Orbea', [42.266950, 2.956106]);
    //         Bicicleta.add(a1, function(err, newBici) {
    //             if (err) console.log(err);
    //             Bicicleta.allBicis(function(err, bicis) {
    //                 expect(bicis.length).toEqual(1);
    //                 expect(bicis[0].code).toEqual(a1.code)
    //                 done();
    //                 console.log('OK! Modelo/Bicicleta.add: Funciona correctamente');
    //             });
    //         });
    //     });
    // });

    describe('Bicicleta.add', () => {
        it('Agrega una Bicicleta', (done) => {
            var a1 = Bicicleta({code: 1, color: 'Black', modelo: 'Orbea', ubicacion: [42.266950, 2.956106]});
            Bicicleta.add(a1, function(err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(a1.code)
                    done();
                    console.log('OK! Modelo/Bicicleta.add: Funciona correctamente');
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Debe devolver la Bicicleta con codigo 1', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);

                var a1 = Bicicleta({code: 1, color: 'Black', modelo: 'Orbea', ubicacion: [42.266950, 2.956106]});
                Bicicleta.add(a1, function(err, newBici) {
                    if (err) console.log(err);

                    var a2 = Bicicleta({code: 2, color: 'White', modelo: 'Cannon', ubicacion: [41, 3]});
                    Bicicleta.add(a2, function(err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function (error, targetBici) {
                            console.log('findbyCode: ' + targetBici);
                            expect(targetBici.code).toBe(a1.code);
                            expect(targetBici.color).toBe(a1.color);
                            expect(targetBici.modelo).toBe(a1.modelo);
                            done();
                            console.log('OK! Modelo/Bicicleta.findByCode: Funciona correctamente');
                        })
                    });
                });
            });
        });
    });

    describe('Bicicleta.removeByCode', () => {
        it('Debe devolver la Bicicleta con codigo 1', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);

                var a1 = Bicicleta({code: 1, color: 'Black', modelo: 'Orbea', ubicacion: [42.266950, 2.956106]});
                Bicicleta.add(a1, function(err, newBici) {
                    if (err) console.log(err);

                    var a2 = Bicicleta({code: 2, color: 'White', modelo: 'Cannon', ubicacion: [41, 3]});
                    Bicicleta.add(a2, function(err, newBici) {
                        if (err) console.log(err);

                        Bicicleta.removeByCode(1, function (err) {
                            if (err) console.log(err);

                            Bicicleta.allBicis(function(err, bicis) {
                                expect(bicis.length).toBe(1);
                                
                                Bicicleta.findByCode(2, function (err, targetBici) {
                                    if (err) console.log(err);
                                    expect(targetBici.code).toBe(a2.code);
                                    expect(targetBici.color).toBe(a2.color);
                                    expect(targetBici.modelo).toBe(a2.modelo);
                                    done();
                                    console.log('OK! Modelo/Bicicleta.findByCode: Funciona correctamente');
                                });
                            });
                        });
                    });
                });
            });
        });
    });

});


/*
beforeEach(() => {
    Bicicleta.allBicis = []
});

describe ('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe ('Bicicleta.add', () => {
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var z = new Bicicleta(1, 'azul', 'montaña', [-34.61, -58,39])
        Bicicleta.add(z);

        //le agrego una para ver si funciona el add
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(z);
    })
})

describe ('Bicicleta.findById', () => {
    it('debe devolver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var bici = new Bicicleta(1, 'verde', 'urbana');
        var bici2 = new Bicicleta(2, 'azul', 'urbana');

        Bicicleta.add(bici);
        Bicicleta.add(bici2);

        var targetBici = Bicicleta.findById(1);

        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(bici.color);
        expect(targetBici.modelo).toBe(bici.modelo);
    })
})

describe ('Bicicleta.removeId', () => {
    it('debe eliminar la bici con id 1', () => {
        var bici = new Bicicleta (1, 'verde', 'montaña');
        Bicicleta.add(bici);

        Bicicleta.removeId(bici.id);

        expect(Bicicleta.allBicis.length).toBe(0);
    })
})
*/