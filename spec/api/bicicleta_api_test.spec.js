var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = 'http://localhost:3000/api/bicicletas';

describe('Bicicleta API', () => {

    beforeAll(function (done) {
        mongoose.connection.close().then(() => {
            mongoose.connect('mongodb://localhost/test', { 
                useUnifiedTopology: true, 
                useFindAndModify: false,
                useCreateIndex: true,
                useNewUrlParser: true
            });
            var db = mongoose.connection;
            db.on('error', console.error.bind(console, 'MongoDB connection error: '));
            db.once('open', function () {
                console.log('We are connected to test database!');
                done();
            });
        });
    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it("Status 200", (done) => {
            request.get(base_url, function (error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toBe(0);
                    done();
                    console.log('OK! api/Bicicletas -> GET : Funciona correctamente');
                });
            })
        })
    })

    describe('POST BICICLETAS /create', () => {
        it("Status 200", (done) => {
            var headers = { 'content-type': 'application/json' };
            var aBici = '{ "code": 10, "color": "Black", "modelo": "Orbea", "lat": 42, "lng": 3 }';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                var result = JSON.parse(body);
                console.log(result);
                expect(result.bicicleta.color).toBe('Black');
                expect(result.bicicleta.modelo).toBe("Orbea");
                expect(result.bicicleta.ubicacion[0]).toBe(42);
                expect(result.bicicleta.ubicacion[1]).toBe(3);
                done();
                console.log('OK! api/bicicletas/create -> POST : Funciona correctamente');
            });
        });
    });

    // describe('Post Bicicletas /update', () => {
    //     it('Status 200', (done) => {
    //         var b1 = new Bicicleta(15, 'Black', 'Orbea', [42, 3]);
    //         Bicicleta.add(b1);

    //         var headers = {'content-type' : 'application/json'};
    //         var a2 = { "id": 15, "color": "Pink", "modelo": "Cannon", "lat": 42, "lng": 3 };
    //         request.post({
    //             headers: headers,
    //             url: 'http://localhost:3000/api/bicicletas/15/update',
    //             body: a2
    //         }, function(error, response, body) {
    //             expect(response.statusCode).toBe(200);
    //             var result = JSON.parse(body);
    //             console.log(result);
    //             expect(result.bicicleta.color).toBe('Pink');
    //             expect(result.bicicleta.modelo).toBe("Cannon");
    //             done();
    //             console.log('OK! api/bicicletas/update -> POST : Funciona correctamente');
    //         });
    //     });
    // });

    // describe('Post Bicicletas /delete', () => {
    //     it('Status 204', (done) => {
    //         var c1 = new Bicicleta(15, 'Black', 'Orbea', [42, 3]);
    //         Bicicleta.add(c1);
    //         // request.get(base_url, function(err, response, body) {
    //         //     var result = JSON.parse(body);
    //         //     expect(response.statusCode).toBe(200);
    //         //     Bicicleta.allBicis(function(err, bicis) {
    //         //         expect(bicis.length).toBe(1);
    //         //         done();
    //         //         console.log('/delete => Añadida Bicicleta');
    //         //     });
    //         // });

    //         var headers = {'content-type' : 'application/json'};
    //         // var a2 = { code: 15 };
    //         request.delete({
    //             headers: headers,
    //             url: base_url + '/delete',
    //             body: { code: 15 }
    //         }, function(err, response, body) {
    //             if (err) console.log('Delete error: ', err);
    //             expect(response.statusCode).toBe(204);
    //             // request.get(base_url, function(error, response, body) {
    //             //     var result = JSON.parse(body);
    //             //     expect(response.statusCode).toBe(200);
    //             //     expect(result.bicicletas.length).toBe(0);
    //                 done();
    //             // });
    //         });
    //     });
    // });


});







/*
beforeEach(() => {
    Bicicleta.allBicis = []
});

describe('Bicicleta API', () => {
    describe('get BICICLETAS', () => {
        it('status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'negro', 'urbana');
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function (error, response, body) {
                expect(response.statusCode).toBe(200);
            }) //es necesario poner function, no funciona la flecha, y la llave tiene que estar pegada al parentesis
        })
    })

    describe('POST BICICLETAS /create', () => {
        it('STATUS 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var aBici = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}'; //es lo mismo que hacer el formato json y pasarlo a string (creo que eso se hace en el proyecto de angular)

            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe('rojo');
                done(); //es lo que espera jasmine para detener el test, algunas veces el test termina antes de que se realiza la peticion POST
            })
        });
    })
})
*/